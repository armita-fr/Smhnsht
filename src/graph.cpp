#include <algorithm>
#include <fstream>
#include <sstream>

#include <QFile>
#include <QTemporaryFile>
#include <QString>

#include "graph.hpp"

heapIter find(heapIter beg, heapIter end, int key)
{
    return std::find_if(beg, end, [key](const heapNode& p) {return p.second == key;});
}

Graph::AdjIter Graph::findNode(AdjIter beg, AdjIter end, int keyCurr, int keyDest) const
{
    return std::find_if(beg, end, [keyCurr, keyDest](const AdjListNode* i)
        {return (((i->dest == keyDest) && (i->curr == keyCurr)) ||
                  (i->dest == keyCurr) && (i->curr == keyDest));});
}

// Adds an edge to an undirected graph
void Graph::addEdge(AdjList* &target, const int& src, const int& dest, const int& weight,
                    const std::string& station, const int& line, const int& dist)
{
	// Add an edge from src to dest. 
	// A new node is added to the adjacency
	// list of src. The node is 
	// added at the beginning
	struct AdjListNode* newNode = 
            newAdjListNode(src, dest, weight, station, line, dist);
    newNode->next = target[src].head;
    target[src].head = newNode;

	// Since graph is undirected, 
	// add an edge from dest to src also
    newNode = newAdjListNode(dest, src, weight, station, line, dist);
    newNode->next = target[dest].head;
    target[dest].head = newNode;
}

// Adds a vertex to an undirected graph
int Graph::addVertex(const std::string& verName)
{
    if (!this->vertices.contains(QString::fromStdString(verName)))
	{
        vertices.append(QString::fromStdString(verName));
        return this->vertices.length() - 1;
	}
    return this->vertices.indexOf(QString::fromStdString(verName));
}

// The main function that calculates best
// time to get from src to all verices.
QStringList Graph::bestTime(const int src,  const int des,  const int time)
{
	// Get the number of vertices in this
	int V = this->V;

	// dist values used to pick
	// minimum weight edge in cut
	std::vector<AdjListNode*> edges;

	// predecessor[i] will hold the predecessor of i
	int predecessor[V];

	// minHeap represents set E
	heap minHeap(V, std::make_pair(INT_MAX, 0));

	// Initialize min heap with all vertices. dist value of all vertices  
	for (int v =  0; v < V; ++v)
	{
		predecessor[v] = -1; // No predecessor found yet
		minHeap[v].second = v;
	}

	// Make dist value of src vertex  0 so that it is extracted first
	minHeap[src].first = 0;
	predecessor[src] = src; // The source vertex is its own predecessor
	std::make_heap(minHeap.begin(), minHeap.end(), std::greater<>{}); // heapify the minHeap vector

	int i{0}; // keep track of how many dists are finalized

	// In the following loop, min heap contains all nodes
	// whose shortest distance is not yet finalized.
	while (minHeap.begin() != (minHeap.end() - i)) // checks if all dists are finalized
	{
		// Extract the vertex with minimum distance value
		// struct MinHeapNode* minHeapNode = extractMin(minHeap);
		std::pop_heap(minHeap.begin(), minHeap.end() - i++, std::greater<>{});
		heapIter minHeapNode = std::prev(minHeap.end(), i);

		// Store the extracted vertex number
		int u = minHeapNode->second;

		// Traverse through all adjacent vertices of u (the extracted vertex)
		// and update their distance values
        struct AdjListNode* pCrawl = this->timeGraph[u].head;

		while (pCrawl != nullptr)
		{
			int v = pCrawl->dest;

			// If shortest distance to v is not finalized yet, and distance to v
			// through u is less than its previously calculated distance

			// finds the node of heap which keeps v and its dist
			heapIter foundV = find(minHeap.begin(), minHeap.end() - i, v);

			if ((foundV != minHeap.end()) && minHeapNode->first != INT_MAX &&
			getWeightOfTrraffic(time + minHeapNode->first, pCrawl) + minHeapNode->first < foundV->first)
			{
				foundV->first = minHeapNode->first + getWeightOfTrraffic(time + minHeapNode->first, pCrawl);
				predecessor[v] = u; // Update the predecessor of v

				// update distance value in min heap also
				// decreaseKey(minHeap, v, dist[v]);
                AdjIter found = findNode(edges.begin(), edges.end(), pCrawl->curr, pCrawl->dest);
                if (found == edges.end()) edges.push_back(pCrawl);
                else *found = pCrawl;
				std::make_heap(minHeap.begin(), minHeap.end() - i, std::greater<>{});
			}
			pCrawl = pCrawl->next;
		}
    }

    int s = des;
    QStringList road;
    while (s != src)
    {
        road.prepend(this->vertices[s]);
        AdjIter found = findNode(edges.begin(), edges.end(), s, predecessor[s]);
        if (found != edges.end())
            road.prepend(QString::fromStdString((*found)->station) + ' ' + QString::number((*found)->line));
        s = predecessor[s];
    }
    road.prepend(this->vertices[src]);

    heapIter found = find(minHeap.begin(), minHeap.end(), des);
    if (found != minHeap.end())
    {
        int timeTaken = found->first;
        emit changeArriving(QString::number((time + timeTaken) / 60  > 23 ? (time + timeTaken) / 60 - 24 : (time + timeTaken) / 60)
                            + ":" + QString::number((time + timeTaken) % 60));
        emit changeCost("-");
        emit changeDistance("-");
    }
    return road;
}

int Graph::getWeightOfTrraffic(const int& time, const AdjListNode *node) const
{
	if ((time >= (6 * 60)) && (time <= (8 * 60))) // Checks if is in bus/subway's traffic hour
	{
		if (node->station == "Bus")
		{
            return (node->time * 2);
		}
		else if (node->station == "Subway")
		{
            return (node->time + 16);
		}
	}
	else if ((time >= (18 * 60)) && (time <= (20 * 60))) // Checks if is in taxi's traffic hour
	{
		if (node->station == "Taxi")
		{
            return (node->time * 2);
		}
	}
    return node->time;
}

// The main function that calculates distances 
// of shortest paths from src to all vertices. 
QStringList Graph::dijkstra(const int src, int des, const int time, const bool& distanceOrPrice)
{
	// Get the number of vertices in this
	int V = this->V;

    const AdjList* array = distanceOrPrice ? this->costGraph : this->distGraph;

	// dist values used to pick
	// minimum weight edge in cut
	std::vector<AdjListNode*> edges;

	// predecessor[i] will hold the predecessor of i
	int predecessor[V];

	// minHeap represents set E
	heap minHeap(V, std::make_pair(INT_MAX, 0));

	// Initialize min heap with all vertices. dist value of all vertices  
	for (int v =  0; v < V; ++v)
	{
		predecessor[v] = -1; // No predecessor found yet
		minHeap[v].second = v;
	}

	// Make dist value of src vertex  0 so that it is extracted first
	minHeap[src].first = 0;
	predecessor[src] = src; // The source vertex is its own predecessor
	std::make_heap(minHeap.begin(), minHeap.end(), std::greater<>{}); // heapify the minHeap vector

	int i{0}; // keep track of how many dists are finalized

	// In the following loop, min heap contains all nodes
	// whose shortest distance is not yet finalized.
	while (minHeap.begin() != (minHeap.end() - i)) // checks if all dists are finalized
	{
		// Extract the vertex with minimum distance value
		// struct MinHeapNode* minHeapNode = extractMin(minHeap);
		std::pop_heap(minHeap.begin(), minHeap.end() - i++, std::greater<>{});
		heapIter minHeapNode = std::prev(minHeap.end(), i);

		// Store the extracted vertex number
		int u = minHeapNode->second;

		// Traverse through all adjacent vertices of u (the extracted vertex)
		// and update their distance values
        struct AdjListNode* pCrawl = array[u].head;

		while (pCrawl != nullptr)
		{
			int v = pCrawl->dest;

			// If shortest distance to v is not finalized yet, and distance to v
			// through u is less than its previously calculated distance

			// finds the node of heap which keeps v and its dist
			heapIter foundV = find(minHeap.begin(), minHeap.end() - i, v);

            if ((foundV != minHeap.end()) && minHeapNode->first != INT_MAX &&
                pCrawl->weight + minHeapNode->first < foundV->first)
			{
				foundV->first = minHeapNode->first + pCrawl->weight;
				predecessor[v] = u; // Update the predecessor of v
                AdjIter found = findNode(edges.begin(), edges.end(), pCrawl->curr, pCrawl->dest);
                if (found == edges.end()) edges.push_back(pCrawl);
                else *found = pCrawl;

				// update distance value in min heap also
				// decreaseKey(minHeap, v, dist[v]);
				
				std::make_heap(minHeap.begin(), minHeap.end() - i, std::greater<>{});
			}
			pCrawl = pCrawl->next;
		}
	}

    QStringList road;
    int arrivingTime = this->roadForDijkstra(des, src, edges, road, predecessor, time, distanceOrPrice);
    road.append(this->vertices[des]);

    emit changeArriving(QString::number(arrivingTime / 60 > 23 ? arrivingTime / 60 - 24 : arrivingTime / 60) + ":" +
                        QString::number(arrivingTime % 60));
    heapIter found = find(minHeap.begin(), minHeap.end(), des);
    if (found != minHeap.end())
    {
        if (distanceOrPrice) {emit changeCost(QString::number(found->first)); emit changeDistance("-");}
        else {emit changeDistance(QString::number(found->first)); emit changeCost("-");}
    }
    return road;
}

// Prepares the road and arriving time of minimum cost/distance road to be printed
int Graph::roadForDijkstra(int s, const int& src, std::vector<AdjListNode*>& edges,
                     QStringList& road, int pre[], const int& time, const bool& distanceOrPrice) const
{
    if (pre[s] == src)
    {
        AdjIter found = findNode(edges.begin(), edges.end(), s, pre[s]);
        if (found != edges.end())
        {
            road.append(this->vertices[pre[s]]);
            road.append(QString::fromStdString((*found)->station) + ' ' + QString::number((*found)->line));
            return time + this->getWeightOfTrraffic(time, (*found));
        }
    }
    AdjIter found = findNode(edges.begin(), edges.end(), s, pre[s]);
    if (found != edges.end())
    {
        int calTime = this->roadForDijkstra(pre[s], src, edges, road, pre, time, distanceOrPrice);
        // checks for distance if the line has changed or if it is for lowest price
        if ((!distanceOrPrice && road.last() != QString::fromStdString((*found)->station) + ' ' + QString::number((*found)->line))
            || distanceOrPrice)
        {
            calTime += this->getWeightOfTrraffic(calTime, (*found));
            road.append(this->vertices[pre[s]]);
            road.append(QString::fromStdString((*found)->station) + ' ' + QString::number((*found)->line));
        }
        else
        {
            if (this->getWeightOfTrraffic(calTime, (*found)) == (*found)->time)
            {
                if ((*found)->station == "Bus") calTime += (*found)->time - 15;
                else if ((*found)->station == "Taxi") calTime += (*found)->time - 5;
                else calTime += (*found)->time - 8;
            }
            else
            {
                if ((*found)->station == "Bus") calTime += this->getWeightOfTrraffic(calTime, (*found)) - 30;
                else if ((*found)->station == "Taxi") calTime += this->getWeightOfTrraffic(calTime, (*found)) - 10;
                else calTime += this->getWeightOfTrraffic(calTime, (*found)) - 24;
            }
        }
        return calTime;
    }
    return 0;
}

void Graph::createGraph()
{
    this->V = 59;
    // Creates individual graphs for dijkstra if time, cost and distance
    this->timeGraph = new struct AdjList[V]{nullptr};
    this->costGraph = new struct AdjList[V]{nullptr};
    this->distGraph = new struct AdjList[V]{nullptr};

    QDir directory(":/file/");
    QStringList txtFiles = directory.entryList(QStringList() << "*.txt" << "*.TXT",QDir::Files);
    foreach(QString filename, txtFiles)
    {
        int distance, line; // Stores weight and line's number of each road
        int source{0}, target{0}; // Stores source and target of each road
        std::string station{""}; // Stores road's type (Bus/Taxi/Subway)
        std::string sen; // Stores each line of the file
        std::string token; // Stores each part of the file's lines

        // Code to acces resource.qrc files with fstream using QTemporaryFile and QFile
        QTemporaryFile tempFile;
        QFile resourceFile(":/file/" + filename);
        tempFile.open();
        resourceFile.open(QIODevice::ReadOnly);
        tempFile.write(resourceFile.readAll());
        resourceFile.close();
        tempFile.close();
        std::ifstream file(tempFile.fileName().toStdString(), std::ios::in);

        std::vector<std::pair<int, int>> vec;
        while (getline(file, sen)) // get from file and putting it in the sen string
        {
            std::istringstream ss(sen);

            // Stores each part of the the line
            std::getline(ss, token, ':'); source = this->addVertex(token);
            std::getline(ss, token, ':'); target = this->addVertex(token);
            std::getline(ss, token, ':'); distance = stoi(token);
            std::getline(ss, token, ':'); station = token;
            std::getline(ss, token, ':'); line = stoi(token);

            vec.push_back(std::make_pair(distance, source));

            if (station == "Taxi_Subway")
                this->addEdge(this->distGraph, source, target, distance, "Subway", line, distance + 8);
            else
                this->addEdge(this->distGraph, source, target, distance, "Bus", line, 4 * distance + 15);
        }
        vec.push_back(std::make_pair(distance, target));
        for(int i = 0; i < vec.size(); ++i)
        {
            int dist{0};
            for(int j = i; j < vec.size() - 1; ++j)
            {
                dist += vec[j].first;
                if (station == "Taxi_Subway")
                {
                    if (dist < 20)
                    this->addEdge(this->timeGraph, vec[i].second, vec[j + 1].second, 2 * dist + 5, "Taxi", line, 2 * dist + 5);
                    this->addEdge(this->timeGraph, vec[i].second, vec[j + 1].second, dist + 8, "Subway", line, dist + 8);
                    this->addEdge(this->costGraph, vec[i].second, vec[j + 1].second, 3267, "Subway", line, dist + 8);
                }
                else
                {
                    this->addEdge(this->timeGraph, vec[i].second, vec[j + 1].second, 4 * dist + 15, "Bus", line, 4 * dist + 15);
                    this->addEdge(this->costGraph, vec[i].second, vec[j + 1].second, 2250, "Bus", line, 4 * dist + 15);
                }
            }
        }
    }
    emit run(this->vertices);
}
