﻿import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Universal
import QtQuick.Window
import org.smhnsht.graph 1.0

ApplicationWindow {
    width: 800
    height: 520
    title: "Smhnsht"
    visible: true
    font.pixelSize: 15

    Universal.accent: Universal.Magenta
    Universal.background: Qt.rgba(0.969, 0.969, 0.969, 1)

    Component.onCompleted: {
        Graph.createGraph();
    }

    Connections
    {
        target: Graph
        function onchangeArriving(time)
        {
            arrivingTime.text = time;
        }
        function onChangeDistance(distance)
        {
            distances.text = distance;
        }
        function onChangeCost(cost)
        {
            costs.text = cost;
        }
        function onRun(vertices)
        {
            start.model = vertices;
            destination.model = vertices;
        }
    }
    Column{
        Item{
            id: topMargin
            width: 12
            height: 5
        }
        spacing: 15
        Row
        {
            Item{
                width: 5
                height: 400
            }

            spacing: 15
            ColumnLayout {
                ColumnLayout {
                    Label {text: "Start:"; font.pointSize: 13; Layout.alignment: Qt.AlignLeft}
                    ComboBox {
                        implicitContentWidthPolicy: ComboBox.WidestText
                        id: start
                    }
                    Label {text: "Destination:"; font.pointSize: 13; Layout.alignment: Qt.AlignLeft}
                    ComboBox {
                        implicitContentWidthPolicy: ComboBox.WidestText
                        id:destination
                    }
                }
                Label {text: "Time:"; font.pointSize: 13; Layout.alignment: Qt.AlignLeft}
                Row{
                    TextField{
                        id:hour
                        placeholderText: "00"
                        validator: RegularExpressionValidator { regularExpression:  /^([0-1]?[0-9]|2[0-3])$ / }
                        width:45
                        height:30
                    }

                    spacing: 2
                    Label{text: ":"; font.pointSize: 15; Layout.alignment: Qt.AlignBottom}
                    TextField
                    {
                        id:min
                        placeholderText: "00"
                        validator: RegularExpressionValidator { regularExpression: /^([0-5]?[0-9]|[6-9])$ / }
                        width:45
                        height:30
                    }
                }
                Column {
                    anchors.left: parent

                    RadioButton
                    {
                        id :dis
                        text: qsTr("Distance")
                        onClicked: {
                            let time = parseInt(hour.text) * 60 + parseInt(min.text);
                            let line = Graph.dijkstra( start.currentIndex, destination.currentIndex, time, 0);
                            path.model = line;
                        }
                    }

                    RadioButton
                    {
                        id:time
                        text: qsTr("Time")
                        onClicked: {
                            let time = parseInt(hour.text) * 60 + parseInt(min.text);
                            let line = Graph.bestTime( start.currentIndex, destination.currentIndex, time);
                            path.model = line;
                        }
                    }

                    RadioButton
                    {
                        id:cost
                        text: qsTr("Cost")
                        onClicked: {
                            let time = parseInt(hour.text) * 60 + parseInt(min.text);
                            let line = Graph.dijkstra( start.currentIndex, destination.currentIndex, time, 1);
                            path.model = line;
                        }
                    }
                }
                ColumnLayout
                {
                    Item{height: 4}

                    Label {text: "Arriving Time:"; font.pixelSize: 15}
                    Text {id: arrivingTime; font.pixelSize: 18; color: Qt.color(Qt.rgba(0.173, 0.616, 0.824, 1)); font.bold: true}

                    Label {text: "Distance:"; font.pixelSize: 15}
                    Text {id: distances; font.pixelSize: 18; color: Qt.color(Qt.rgba(0.937,0.69,0, 1)); font.bold: true}

                    Label {text: "Cost:"; font.pixelSize: 15}
                    Text {id: costs ; font.pixelSize: 18; color: Qt.color(Qt.rgba(0.153, 0.745, 0.078, 1)); font.bold: true}
                }
            }
            Item {
                width:24
                height:400
            }
        }
    }
    Row{
        Label {id: l1;
            anchors.fill: parent;
            anchors.leftMargin: destination.width + 50;
            anchors.topMargin: destination.height;
            text: "Direction:"; font.pointSize: 13;
        }
    }
    Flow{

        anchors.fill: parent
        anchors.leftMargin: destination.width + 50
        anchors.topMargin: destination.height + l1.height + 65
        Layout.fillHeight: true
        Layout.fillWidth: true
        enabled: visible
        spacing: 5
        Repeater{
            id: path
            Rectangle { width: modelData.length * 10;
                height: 40; radius: 15;
                color: index % 2 === 1 ?  Qt.rgba(0.827, 0.082, 0.357, 1) : Qt.rgba(0.996, 0.051, 0.388, 1);
                Text {
                    text:modelData
                    font.pixelSize: 13
                    color: "white"
                    anchors.centerIn: parent
                }
            }
        }
    }
}
