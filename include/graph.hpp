// Graph class definition; Member functions defined in Graph.cpp

#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <limits.h>
#include <string>
#include <iostream>
#include <vector>
#include <utility>

#include <QStringList>
#include <QObject>
#include <QtQml>

typedef std::vector<std::pair<int, int>> heap;
typedef std::pair<int, int> heapNode;
typedef heap::iterator heapIter;

heapIter find(heapIter beg, heapIter end, int key);

class Graph : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

private:
    struct AdjListNode
	{
		std::string station;
		int line;
		int dest;
		int curr;
		int weight;
        int time;
		struct AdjListNode* next;
	};

    typedef std::vector<AdjListNode*>::iterator AdjIter;
    AdjIter findNode(AdjIter, AdjIter, int, int) const;

    struct AdjListNode* newAdjListNode(
                    int src, int dest, int weight, std::string station, int line, int time)
	{
		struct AdjListNode* newNode = new AdjListNode;
		newNode->dest = dest;
		newNode->curr = src;
		newNode->weight = weight;
		newNode->station = station;
		newNode->line = line;
        newNode->time = time;
		newNode->next = nullptr;
		return newNode;
	}

    struct AdjList { struct AdjListNode *head{nullptr}; };

	int V;
    QStringList vertices;
    struct AdjList* distGraph;
    struct AdjList* costGraph;
    struct AdjList* timeGraph;

    void addEdge(AdjList*&, const int&, const int&, const int&, const std::string&, const int&, const int&);
    int addVertex(const std::string&);
    int getWeightOfTrraffic(const int&, const AdjListNode*) const;
    int roadForDijkstra(int, const int&, std::vector<AdjListNode*>&, QStringList&, int[], const int&, const bool&) const;

public slots:
    QStringList bestTime(const int, const int, const int);
    QStringList dijkstra(const int, int, const int, const bool&); // if 1 do dijkstra for price and if 0 do dijkstra for distance
    void createGraph();

signals:
    void changeDistance(QString distance);
    void changeCost(QString cost);
    void changeArriving(QString time);
    void run(QStringList vertices);
};

#endif
